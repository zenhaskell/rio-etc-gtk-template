# rio-etc-gtk-template

A comprehensive stack project utilizing the
[RIO](https://hackage.haskell.org/package/rio) prelude, the
[etc](https://hackage.haskell.org/package/etc) configuration manager
and [gt-gtk](https://hackage.haskell.org/package/gi-gtk). This
builds in gitlab's ci to produce an output binary.

## Building

Build with

    stack build

Run with

    stack exec -- app-exe
